from __future__ import unicode_literals

import datetime
import logging

from django.conf import settings
from django.core.cache import cache
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, post_delete
from django.utils import timezone

logger = logging.getLogger('default')


class BannedIPManager(models.Manager):
    def ban(self, ip, seconds=None, ban_permanent=False):
        """
        Ban's IP
        :param ip: required
        :param seconds: if seconds is none then banned based on DEFAULT_BAN_TIME_IN_SECS default settings variable.
        :param ban_permanent:
        :return:
        """
        seconds = seconds if seconds else settings.DEFAULT_BAN_TIME_IN_SECS
        ban_time = timezone.now() + datetime.timedelta(seconds=seconds)
        (ip, is_created) = self.update_or_create(ip=ip,
                                                 defaults=dict(
                                                     ban_permanent=ban_permanent,
                                                     ban_time=ban_time,
                                                 ))
        return ip

    def un_ban(self, ip):
        if self.filter(ip=ip).exists():
            self.get(ip=ip).update(is_banned=False)


class BannedIP(models.Model):
    ip = models.CharField(max_length=256)

    is_banned = models.BooleanField(default=False)
    ban_time = models.DateTimeField(default=None)
    added_at = models.DateTimeField(auto_now_add=True)
    ban_permanent = models.BooleanField(default=False)

    objects = BannedIPManager()

    def ban_permanently(self):
        self.ban_permanent = True
        self.save()
        return self


def _update_cache(sender, instance, **kwargs):
    block_ips = BannedIP.objects. \
        filter(Q(is_banned=True) | Q(ban_permanent=True)). \
        values_list('ip', flat=True).distinct()
    cache.set('blockip:list', block_ips)

post_save.connect(_update_cache, sender=BannedIP)
post_delete.connect(_update_cache, sender=BannedIP)