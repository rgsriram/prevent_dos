# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-11-27 11:28
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ip', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bannedip',
            name='is_active',
        ),
        migrations.RemoveField(
            model_name='bannedip',
            name='source',
        ),
    ]
