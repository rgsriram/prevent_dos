import logging
from django.conf import settings
from django.http import HttpResponseNotFound
from django.shortcuts import render, redirect

from forms import CaptchaForm
from models import BannedIP

logger = logging.getLogger('default')


def get_captcha_form():
    return CaptchaForm()


def post_captcha_form(request):
    form = CaptchaForm(request.POST)
    if form.is_valid():
        return True


def captcha_view(request):
    if request.POST:
        result = post_captcha_form(request)
        if result:
            request.session['is_human'] = True
            return redirect('/ips/')
    else:
        form = get_captcha_form()

    return render(request, 'web/index.html', locals())


def ip_list_view(request):
    if not request.session.get('is_human'):
        return HttpResponseNotFound('<h1> Not found </h1>')

    ips = BannedIP.objects.filter(is_banned=True)
    return render(request, 'web/ips.html', {'ips': ips})


def ban_ip(ip):
    return BannedIP.objects.filter(ip=ip).update(is_banned=True)


def ip_release_view(request, ip):
    """
    1. If captcha is passed, then ip will be released
    2. If captcha is failed for certain no of times (refer: CONFIG_TO_BAN_IP settings variable) then ip will be banned.
    :param request:
    :param ip:
    :return:
    """
    counter = 0
    if request.POST:
        result = post_captcha_form(request)
        if result:
            try:
                BannedIP.objects.get(ip=ip)
                BannedIP.objects.un_ban(ip)
                request.session['is_human'] = True
            except BannedIP.DoesNotExist:
                return HttpResponseNotFound('<h1> Not found </h1>')
        elif counter >= settings.CONFIG_TO_BAN_IP:
            ban_ip(ip)
        return redirect('/ips/')
    else:
        request.session['is_human'] = False
        form = get_captcha_form()

    counter += 1
    return render(request, 'web/index.html', locals())


def ip_ban_view(request, ip):
    if request.POST:
        result = post_captcha_form(request)
        if result:
            try:
                banned_ip = BannedIP.objects.get(ip=ip)
                banned_ip.ban_permanently()
                request.session['is_human'] = True
            except BannedIP.DoesNotExist:
                return HttpResponseNotFound('<h1> Not found </h1>')

            return redirect('/ips/')
    else:
        request.session['is_human'] = False
        form = get_captcha_form()

    return render(request, 'web/index.html', locals())

