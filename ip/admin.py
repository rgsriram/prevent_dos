from django.contrib import admin
from models import BannedIP


class BannedIPAdmin(admin.ModelAdmin):
    list_display = ('ip', 'ban_time', 'added_at', 'is_banned', 'ban_permanent')


admin.site.register(BannedIP, BannedIPAdmin)