from django.conf.urls import url

from views import captcha_view, ip_list_view, ip_release_view

urlpatterns = [
    url(r'ips/release/(?P<ip>[\d.]+)/', ip_release_view),
    url(r'ips/$', ip_list_view),
    url(r'$', captcha_view),
]