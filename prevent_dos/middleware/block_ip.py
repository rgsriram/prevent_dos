from django.core.cache import cache
from django.db.models import Q
from django.http import HttpResponseForbidden

from ip.models import BannedIP


def get_ip(req):
    return req.META['REMOTE_ADDR']


class BanIPMiddleware(object):
    def process_request(self, request):
        """
        Ban IP if it is present in block ip list.
        :param request:
        :return:
        """
        ip = get_ip(request)
        block_ips = cache.get('blockip:list')

        if not block_ips:
            block_ips = BannedIP.objects.\
                filter(Q(is_banned=True) | Q(ban_permanent=True)).\
                values_list('ip', flat=True).distinct()
            cache.set('blockip:list', block_ips)

        # If IP is found in block list returns "Forbidden Access"
        if ip in block_ips:
            for k in request.session.keys():
                del request.session[k]
            return HttpResponseForbidden("")