import datetime

from django.conf import settings
from django.core.cache import cache
from django.utils import timezone

from ip.models import BannedIP


class DetectDOSMiddleware(object):
    def process_request(self, request):
        """
        How it works?: If number of connections increased in last certain time frame then
                        IP will be added to block List.
        :param request:
        :return:
        """
        ip = self.get_ip(request)
        connections = cache.get(ip)

        if not connections:
            cache.set(ip, 0, settings.EXPIRY_TIME_IN_SECS)

        cache.incr(ip)

        # CONNECTIONS_THRESHOLD configurable settings variable to allow max number of connections within time frame.
        if cache.get(ip) >= settings.CONNECTIONS_THRESHOLD:
            BannedIP.objects.ban(ip)

    def get_ip(self, req):
        """
        Function to find ip address
        :param req: request
        :return: ip
        """
        return req.META['REMOTE_ADDR']



