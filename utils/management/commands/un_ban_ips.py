import datetime

from django.conf import settings
from django.core.management.base import BaseCommand
from django.utils import timezone

from ip.models import BannedIP


class UnBanIP(BaseCommand):
    def handle(self, *args, **options):
        cutoff = timezone.now() - datetime.timedelta(minutes=settings.DEFAULT_BAN_TIME_IN_SECS)
        qs = BannedIP.objects.filter(added_at__lte=cutoff, ban_permanent=False, is_banned=True)

        qs.update(is_banned=False)

